﻿-- View: sig.vpcpopperfil

-- DROP VIEW sig.vpcpopperfil;

CREATE OR REPLACE VIEW sig.vpcpopperfil AS 
 SELECT a.numero,
    a.data,
    a.num_pedido,
    a.prioridade,
    b.descricao,
    b.aplicacao,
    a.codigo,
    a.num_ferramenta,
    a.tamanho,
    a.qtdade,
    a.peso_perfil,
    a.peso_ferramenta,
    a.tam_puxada,
    a.tam_tarugo,
    a.qtd_tarugo,
    a.id_pcpopperfil,
    a.id_empresa,
    a.situacao
   FROM sig.pcpopperfil a
     LEFT JOIN sig.pcpperfil b ON a.codigo = b.codigo AND a.id_empresa = b.id_empresa;

ALTER TABLE sig.vpcpopperfil
  OWNER TO postgres;
GRANT ALL ON TABLE sig.vpcpopperfil TO postgres;
GRANT SELECT ON TABLE sig.vpcpopperfil TO sysdatatec;
