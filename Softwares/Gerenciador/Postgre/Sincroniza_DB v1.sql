﻿--Atualiza a tabela caixa
CREATE OR REPLACE FUNCTION transelevador.copia_caixa() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.altr_caixa  
		SET 	
			id_caixa 	= NEW.id_caixa,
			numero 		= NEW.numero,
			tipo_caixa 	= NEW.tipo_caixa,
			rfid 		= NEW.rfid,
			vol_m3 		= NEW.vol_m3,
			tot_req 	= NEW.tot_req,
			vol_m3_disp 	= NEW.vol_m3_disp,
			perc_ocup 	= NEW.perc_ocup 
		WHERE 	
			id_caixa = NEW.id_caixa;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.altr_caixa 
			VALUES (
				NEW.id_caixa, 
				NEW.numero, 
				NEW.tipo_caixa, 
				NEW.rfid, 
				NEW.vol_m3, 
				NEW.tot_req, 
				NEW.vol_m3_disp, 
				NEW.perc_ocup
				); 
		END IF; 
	RETURN NEW;
	END'
LANGUAGE plpgsql;

--Atualiza a tabela caixadet
CREATE OR REPLACE FUNCTION transelevador.copia_caixadet() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.altr_caixadet  
		SET 	
			id_caixadet 	= NEW.id_caixadet,
			id_caixa 	= NEW.id_caixa,
			divisao 	= NEW.divisao,
			id_produto 	= NEW.id_produto,
			estoque 	= NEW.estoque,
			em_uso 		= NEW.em_uso
		WHERE 	
			id_caixadet = NEW.id_caixadet;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.altr_caixadet 
			VALUES (
				NEW.id_caixadet,
				NEW.id_caixa,
				NEW.divisao,
				NEW.id_produto,
				NEW.estoque,
				NEW.em_uso
				); 
		END IF; 
	RETURN NEW;
	END'
LANGUAGE plpgsql;

--Atualiza a tabela ordem
CREATE OR REPLACE FUNCTION transelevador.copia_ordem() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.altr_ordem  
		SET 	
			id_ordem 	=  NEW.id_ordem,
			numero 		=  NEW.numero,
			tipo 		=  NEW.tipo,
			status 		=  NEW.status,
			temp_prev_exec 	=  NEW.temp_prev_exec,
			hr_ini 		=  NEW.hr_ini,
			hr_fim 		=  NEW.hr_fim,
			prazo_entrega 	=  NEW.prazo_entrega,
			data 		=  NEW.data,
			dt_conclusao 	=  NEW.dt_conclusao,
			temp_parada 	=  NEW.temp_parada,
			origem_id 	=  NEW.origem_id,
			origem_ordem 	=  NEW.origem_ordem,
			origem_prefixo 	=  NEW.origem_prefixo,
			origem	 	=  NEW.origem
		WHERE 	
			id_ordem = NEW.id_ordem;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.altr_ordem
			VALUES (
				NEW.id_ordem,
				NEW.numero,
				NEW.tipo,
				NEW.status,
				NEW.temp_prev_exec,
				NEW.hr_ini,
				NEW.hr_fim,
				NEW.prazo_entrega,
				NEW.data,
				NEW.dt_conclusao,
				NEW.temp_parada,
				NEW.origem_id,
				NEW.origem_ordem,
				NEW.origem_prefixo,
				NEW.origem
				); 
		END IF; 
	RETURN NEW;
	END'
LANGUAGE plpgsql;

--Atualiza a tabela ordemdet
CREATE OR REPLACE FUNCTION transelevador.copia_ordemdet() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.altr_ordemdet  
		SET 	
			id_ordemdet =  NEW.id_ordemdet,
			id_ordem =  NEW.id_ordem,
			id_produto =  NEW.id_produto,
			id_caixadet =  COALESCE(NEW.id_caixadet,0),
			qtd_prevista =  NEW.qtd_prevista,
			qtd_atendida =  NEW.qtd_atendida,
			status =  NEW.status
		WHERE 	
			id_ordemdet = NEW.id_ordemdet;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.altr_ordemdet
			VALUES (
				NEW.id_ordemdet,
				NEW.id_ordem,
				NEW.id_produto,
				COALESCE(NEW.id_caixadet,0),
				NEW.qtd_prevista,
				NEW.qtd_atendida,
				NEW.status
				); 
		END IF; 
	RETURN NEW;
	END'
LANGUAGE plpgsql;

--Atualiza a tabela produto
CREATE OR REPLACE FUNCTION transelevador.copia_produto() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.altr_produto  
		SET 	
			id_produto =  NEW.id_produto,
			id_empresa =  NEW.id_empresa,
			codigo =  NEW.codigo,
			descricao =  NEW.descricao,
			referencia =  NEW.referencia,
			cod_barra =  NEW.cod_barra,
			unidade =  NEW.unidade,
			data_cadastro =  NEW.data_cadastro,
			ativo =  NEW.ativo,
			peso_liquido =  NEW.peso_liquido,
			peso_bruto =  NEW.peso_bruto,
			largura =  NEW.largura,
			altura =  NEW.altura,
			comprimento =  NEW.comprimento,
			empilhamento =  NEW.empilhamento,
			tipo_caixa =  NEW.tipo_caixa,
			vol_m3 =  NEW.vol_m3,
			qtd_caixa =  NEW.qtd_caixa
		WHERE 	
			id_produto = NEW.id_produto;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.altr_produto
			VALUES (
				NEW.id_produto,
				NEW.id_empresa,
				NEW.codigo,
				NEW.descricao,
				NEW.referencia,
				NEW.cod_barra,
				NEW.unidade,
				NEW.data_cadastro,
				NEW.ativo,
				NEW.peso_liquido,
				NEW.peso_bruto,
				NEW.largura,
				NEW.altura,
				NEW.comprimento,
				NEW.empilhamento,
				NEW.tipo_caixa,
				NEW.vol_m3,
				NEW.qtd_caixa
				); 
		END IF; 
	RETURN NEW;
	END'
LANGUAGE plpgsql;


--Atualiza a tabela ordemdet
CREATE OR REPLACE FUNCTION transelevador.copia_prodxcaixa() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.altr_prodxcaixa
		SET 	
			id_prodxcaixa =  NEW.id_prodxcaixa,
			id_produto =  NEW.id_produto,
			id_caixa =  NEW.id_caixa,
			qtdade =  NEW.qtdade
		WHERE 	
			id_prodxcaixa = NEW.id_prodxcaixa;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.altr_prodxcaixa
			VALUES (
				NEW.id_prodxcaixa,
				NEW.id_produto,
				NEW.id_caixa,
				NEW.qtdade
				); 
		END IF; 
	RETURN NEW;
	END'
LANGUAGE plpgsql;

--***********************************************************************
--Deleta informações antigas da tabela caixa
CREATE OR REPLACE FUNCTION transelevador.deleta_caixa() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.del_caixa  
		SET 	
			id_caixa 	= OLD.id_caixa
		WHERE 	
			id_caixa = OLD.id_caixa;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.del_caixa 
			VALUES (
				OLD.id_caixa
				); 
		END IF; 
	RETURN OLD;
	END'
LANGUAGE plpgsql;

--Deleta informações antigas da tabela caixadet
CREATE OR REPLACE FUNCTION transelevador.deleta_caixadet() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.del_caixadet  
		SET 	
			id_caixadet 	= OLD.id_caixadet
		WHERE 	
			id_caixadet = OLD.id_caixadet;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.del_caixadet 
			VALUES (
				OLD.id_caixadet
				); 
		END IF; 
	RETURN OLD;
	END'
LANGUAGE plpgsql;

--Deleta informações antigas da tabela ordem
CREATE OR REPLACE FUNCTION transelevador.deleta_ordem() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.del_ordem  
		SET 	
			id_ordem 	=  OLD.id_ordem
		WHERE 	
			id_ordem = OLD.id_ordem;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.del_ordem
			VALUES (
				OLD.id_ordem
				); 
		END IF; 
	RETURN OLD;
	END'
LANGUAGE plpgsql;

--Deleta informações antigas da tabela ordemdet
CREATE OR REPLACE FUNCTION transelevador.deleta_ordemdet() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.del_ordemdet  
		SET 	
			id_ordemdet =  OLD.id_ordemdet
		WHERE 	
			id_ordemdet = OLD.id_ordemdet;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.del_ordemdet
			VALUES (
				OLD.id_ordemdet
				); 
		END IF; 
	RETURN OLD;
	END'
LANGUAGE plpgsql;

--Deleta informações antigas da tabela produto
CREATE OR REPLACE FUNCTION transelevador.deleta_produto() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.del_produto  
		SET 	
			id_produto =  OLD.id_produto
		WHERE 	
			id_produto = OLD.id_produto;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.del_produto
			VALUES (
				OLD.id_produto
				); 
		END IF; 
	RETURN OLD;
	END'
LANGUAGE plpgsql;

--Deleta informações antigas da tabela produto
CREATE OR REPLACE FUNCTION transelevador.deleta_prodxcaixa() 
RETURNS trigger AS '
	BEGIN
		UPDATE transelevador.del_prodxcaixa 
		SET 	
			id_prodxcaixa =  OLD.id_prodxcaixa
		WHERE 	
			id_prodxcaixa = OLD.id_prodxcaixa;
		IF NOT FOUND THEN 
			INSERT INTO transelevador.del_prodxcaixa
			VALUES (
				OLD.id_prodxcaixa
				); 
		END IF; 
	RETURN OLD;
	END'
LANGUAGE plpgsql;

--Cria triggers de atualização
CREATE TRIGGER atualiza_caixa
    AFTER UPDATE ON transelevador.caixa
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE transelevador.copia_caixa();

CREATE TRIGGER atualiza_caixadet
    AFTER UPDATE ON transelevador.caixadet
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE transelevador.copia_caixadet();

CREATE TRIGGER atualiza_ordem
    AFTER UPDATE ON transelevador.ordem
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE transelevador.copia_ordem();

CREATE TRIGGER atualiza_ordemdet
    AFTER UPDATE ON transelevador.ordemdet
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE transelevador.copia_ordemdet();

CREATE TRIGGER atualiza_produto
    AFTER UPDATE ON transelevador.produto
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE transelevador.copia_produto();

CREATE TRIGGER atualiza_prodxcaixa
    AFTER UPDATE ON transelevador.prodxcaixa
    FOR EACH ROW
    WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE transelevador.copia_prodxcaixa();

--Cria triggers de inserção
CREATE TRIGGER atualiza_caixa_isr
    AFTER INSERT ON transelevador.caixa
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.copia_caixa();

CREATE TRIGGER atualiza_caixadet_isr
    AFTER INSERT ON transelevador.caixadet
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.copia_caixadet();

CREATE TRIGGER atualiza_ordem_isr
    AFTER INSERT ON transelevador.ordem
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.copia_ordem();

CREATE TRIGGER atualiza_ordemdet_isr
    AFTER INSERT ON transelevador.ordemdet
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.copia_ordemdet();

CREATE TRIGGER atualiza_produto_isr
    AFTER INSERT ON transelevador.produto
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.copia_produto();

CREATE TRIGGER atualiza_prodxcaixa_isr
    AFTER INSERT ON transelevador.prodxcaixa
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.copia_prodxcaixa();

--Cria triggers de apagar informações
CREATE TRIGGER atualiza_caixa_del
    BEFORE DELETE ON transelevador.caixa
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.deleta_caixa();

CREATE TRIGGER atualiza_caixadet_del
    BEFORE DELETE ON transelevador.caixadet
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.deleta_caixadet();

CREATE TRIGGER atualiza_ordem_del
    BEFORE DELETE ON transelevador.ordem
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.deleta_ordem();

CREATE TRIGGER atualiza_ordemdet_del
    BEFORE DELETE ON transelevador.ordemdet
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.deleta_ordemdet();

CREATE TRIGGER atualiza_produto_del
    BEFORE DELETE ON transelevador.produto
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.deleta_produto();

CREATE TRIGGER atualiza_prodxcaixa_del
    BEFORE DELETE ON transelevador.prodxcaixa
    FOR EACH ROW
    EXECUTE PROCEDURE transelevador.deleta_prodxcaixa();