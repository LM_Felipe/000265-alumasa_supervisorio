;*************************************************************************************************************************************************************
;Este programa tem como finalidade copiar as informa��es do DB Postgre do Tid�o para um DB Local MSSql
;Esta opera��o visa facilitar o uso das informa��es do DB utilizando uma conec��o nativa do Elipse

;Data da elabora��o: Abril-2017
;Data de revis�o: Outubro-2021
;*************************************************************************************************************************************************************

#include <Date.au3>
#include <File.au3>
#include <Misc.au3>

;For�a rodar s� um script por vez
_Singleton("1")

;Define a fun��o de retorno quando ocorre um erro
Local $oMyError = ObjEvent("AutoIt.Error", "ErrFunc")

;Habilta a leitura e escrita nos dois sentidos
$habilita_bidirecional = true

;Loop infinito
While True

TraySetToolTip ( "Conector Alumasa - LMLogix" & @CRLF & _
			     "�ltima execu��o: " & _NowTime ( 5 ) )

ConsoleWrite(_NowTime ( 5 ) & " - Iniciado ciclo" & @CRLF)

;-------------------------------------------------------------------------
;-----------------------Se conecta com DB MSsql---------------------------
;-------------------------------------------------------------------------

;Objeto de conex�o (Escrita)
$msConn 	= ObjCreate("ADODB.Connection")
$msPk 		= ObjCreate("ADODB.Connection")
$msConn_del	= ObjCreate("ADODB.Connection")

;Objeto de Recordset (Leitura)
$msRS 		= ObjCreate("ADODB.Recordset")
$msRS_del	= ObjCreate("ADODB.Recordset")
$msPkRS 	= ObjCreate("ADODB.Recordset")
$msTarRS 	= ObjCreate("ADODB.Recordset")

;String de conex�o
$msStrConn = "DRIVER={SQL Server};" & _
			 "SERVER=EXTRUSAO3-PC\SQLEXPRESS;" & _
			 "DATABASE=Alumasa;" & _
			 "Uid=sa;" & _
             "Pwd=@Logix161516#;"

;Abre as conex�es
$msConn.Open ($msStrConn) 		;Consulta de valores
$msPk.Open ($msStrConn)   		;Consulta de PK
$msConn_del.Open ($msStrConn)	;Consulta para deletar

;-------------------------------------------------------------------------
;---------------Se conecta com DB Postgre --------------------------------
;-------------------------------------------------------------------------

;Objeto de conex�o (Escrita)
$pgConn 	= ObjCreate("ADODB.Connection")
$pgPk 		= ObjCreate("ADODB.Connection")
$pgConn_del = ObjCreate("ADODB.Connection")

;Objeto de Recordset (Leitura)
$pgRS 		= ObjCreate("ADODB.Recordset")
$pgPkRS 	= ObjCreate("ADODB.Recordset")
$pgRS_del 	= ObjCreate("ADODB.Recordset")

;String de conex�o
$pgStrConn = "DRIVER={PostgreSQL ANSI};" & _
			 "DATABASE=alumasa_f;" & _
			 "SERVER=192.168.1.99;" & _
			 "PORT=5432;" & _
			 "Uid=postgres;" & _
			 "Pwd=#jcs@2017#;"

;Abre as conex�es
$pgConn.Open ($pgStrConn)		;Consulta de valores
$pgPk.Open ($pgStrConn)			;Consulta de PK
$pgConn_del.Open ($pgStrConn)	;Consulta para deletar

;--------------------------------------------------------------------------------------------------------------------------------------------------
;                                          SINCRONIZA��O POSTGRE >>>>>>>>>> SQL SERVER
;--------------------------------------------------------------------------------------------------------------------------------------------------

;-------------------------------------------------------------------------
;------------Escreve informa��es do MSsql para o Postgre------------------
;-------------------------------------------------------------------------


$matriz_tabelas = "sig.pcpferramenta,sig.pcpopperfil,sig.pcpperfil,sig.pcppeso" ;Tabelas a serem copiadas, na origem ser�o copiadas as tabelas altr_

;Separa as tabelas em uma matriz
$lista_tabelas = StringSplit($matriz_tabelas, ",")

for $i = 1 to $lista_tabelas[0]

;Nome da tabela no Postgre
$tabela = $lista_tabelas[$i]

;Nome da tabela no MS Sql
$table_name = StringSplit($tabela, ".")

;Nome da tabela no Postgre com prefixo altr_
$tabela = StringReplace($tabela,".",".altr_")

;Query para identificar uma PK no MS Sql
$msFindPkString = "SELECT COLUMN_NAME FROM Alumasa.INFORMATION_SCHEMA.KEY_COLUMN_USAGE " & _
				  "WHERE TABLE_NAME LIKE '" & $table_name[2] & "' AND CONSTRAINT_NAME LIKE 'PK%'"

;Query para identificar uma PK no Postgre
$pgFindPkString = "select kcu.table_schema," & _
"       kcu.table_name," & _
"       tco.constraint_name," & _
"       kcu.ordinal_position as position," & _
"       kcu.column_name as key_column" & _
" from information_schema.table_constraints tco" & _
" join information_schema.key_column_usage kcu " & _
"     on kcu.constraint_name = tco.constraint_name" & _
"     and kcu.constraint_schema = tco.constraint_schema" & _
"     and kcu.constraint_name = tco.constraint_name" & _
" where tco.constraint_type = 'PRIMARY KEY' AND kcu.table_name = '"& $table_name[2] & "'" & _
" order by kcu.table_schema," & _
"         kcu.table_name," & _
"         position;"

;Query insert OR update
   $str = "SET IDENTITY_INSERT #!#tabela#!# ON " & @CRLF & _
		  "IF EXISTS (SELECT * FROM #!#tabela#!# WHERE #!#PK#!# = #!#valorPK#!#) " & @CRLF & _
		  "BEGIN " & @CRLF & _
		  "UPDATE #!#tabela#!# " & @CRLF & _
		  "SET #!#valorupdate#!# " & @CRLF & _
          "WHERE #!#PK#!# = '#!#valorPK#!#'; " & @CRLF & _
		  "END " & @CRLF & _
    "ELSE  " & @CRLF & _
	"BEGIN " & @CRLF & _
        "INSERT INTO #!#tabela#!# (#!#colunas#!#) VALUES (#!#valores#!#); " & @CRLF & _
    "END" & @CRLF & _
	"SET IDENTITY_INSERT #!#tabela#!# OFF "

;Query para deletar informa��es ap�s copiar
$str_query_delete = "DELETE FROM #!#tabela#!# WHERE #!#PK#!# = #!#valorPK#!#"

ConsoleWrite(_NowTime ( 5 ) & " - Postgre >> MSSql - Copiando informa��es da tabela " & $tabela & @CRLF)

;Pega todas as informa��es da tabela
$pgRS.Open( _
"SELECT * " & _
"FROM " & $tabela & ";", _
$pgConn, 1, 3)

;Consulta a PK
$msPkRS.Open( $msFindPkString, $msPk, 1, 3)
$msPkRS.MoveFirst()

;Escreve na v�riavel a PK
for $oField In $msPkRS.Fields
   $TablePk = $oField.Value
next

;Lista todos os campos do DB
$fields = ""
for $oField In $pgRS.Fields
$fields &= $oField.Name & ","
next

;Registra na variavel todas as colunas
$fields = StringTrimRight($fields, 1)

;Pega os valores do Postgre e escreve no MSSQL
If ((Not $pgRS.EOF) AND (Not $pgRS.BOF)) Then

;Vai para o primeiro registro do recordset
$pgRS.MoveFirst()

;Cria as variaveis de valor e PK
$valor = ""
$PKvalor = ""

;Percorre todo o DB MSsql pegando o valor de cada linha e montando um insert or update
While Not $pgRS.EOF

   ;Variavel do insert
   $valor = ""
   ;Variavel do UPDATE
   $updateString = ""

   ;Faz o loop das colunas para achar o valor da PK e montar a string de update
   for $oField In $pgRS.Fields

	  $convertedValue = ConvertField( $oField )
	  ;Verifica se a coluna atual � a PK
	  if $oField.Name == $TablePk Then
		 ;Registra qual o valor da PK
		 $PKvalor = $convertedValue
	  else
		 ;Monta a string com os valores de update
		 $updateString &= $oField.Name & " = " & $convertedValue & ","
	  EndIf

	  ;Monta a string com os valores
	  $valor &= $convertedValue & ","

   next

   ;Remove as virgulas
   $valor = StringTrimRight ( $valor, 1 )
   $updateString = StringTrimRight ( $updateString, 1 )

   ;Substitui as variaveis da querry pelos valores da consulta
   $query = StringReplace($str,   "#!#tabela#!#", $table_name[2])
   $query = StringReplace($query, "#!#PK#!#", $TablePk)
   $query = StringReplace($query, "#!#valorPK#!#", $PKvalor)
   $query = StringReplace($query, "#!#valorupdate#!#", $updateString)
   $query = StringReplace($query, "#!#colunas#!#", $fields)
   $query = StringReplace($query, "#!#valores#!#", $valor)

   ;Substitui as variaveis da querry delete
   $query_delete = StringReplace($str_query_delete, "#!#tabela#!#", $tabela)
   $query_delete = StringReplace($query_delete, "#!#PK#!#", $TablePk)
   $query_delete = StringReplace($query_delete, "#!#valorPK#!#", $PKvalor)

   ConsoleWrite(_NowTime ( 5 ) & " - Postgre >> MSSql - Nova informa��o registrada na tabela: " & $tabela & @CRLF)

   ;Grava as informa��es na tabela do SQL Server
   ConsoleWrite(_NowTime ( 5 ) & " - Querry:"& @CRLF & $query & @CRLF)
   $msConn.execute ( $query )

   ;Apaga informa��es da tabela espelho
   ConsoleWrite(_NowTime ( 5 ) & " - Querry:"& @CRLF & $query_delete & @CRLF)
   $pgConn_del.execute ( $query_delete )

   ;Pr�ximo registro da tabela altr
   $pgRS.MoveNext()

WEnd

EndIf

;Fecha os RecordSets
$pgRS.Close
$msPkRS.Close

Next ;Next para pr�xima tabela

;--------------------------------------------------------------------------------------------------------------------------------------------------
;                                          SINCRONIZA��O SQL SERVER >>>>>>>>>> POSTGRE
;--------------------------------------------------------------------------------------------------------------------------------------------------

;Verifica se � para copiar informa��es de volta para o DB Postgre
If $habilita_bidirecional Then

;Tabelas para serem copiadas do SQL Server pro Postgre
$matriz_tabelas = "sig.pcpopperfil,sig.pcppeso"

;Divide a string pegando as tabelas individuais
$lista_tabelas = StringSplit($matriz_tabelas, ",")

for $i = 1 to $lista_tabelas[0]

;Define qual tabela ser� consultada
$tabela = $lista_tabelas[$i]

;Procura qual � a PK da tabela
$table_name = StringSplit($tabela, ".")

;Como a tabela do trigger n�o tem PK tenho que pegar a PK do destino
$table_pk = $table_name[2]

;Tabela fonte de dados
$table_name[2] = "altr_" & $table_name[2]

;Query para identificar uma PK no MS Sql
$msFindPkString = "SELECT COLUMN_NAME FROM Alumasa.INFORMATION_SCHEMA.KEY_COLUMN_USAGE " & _
				  "WHERE TABLE_NAME LIKE '" & $table_name[2] & "' AND CONSTRAINT_NAME LIKE 'PK%'"

;Query para identificar uma PK no Postgre
$pgFindPkString = "select kcu.table_schema," & _
"       kcu.table_name," & _
"       tco.constraint_name," & _
"       kcu.ordinal_position as position," & _
"       kcu.column_name as key_column" & _
" from information_schema.table_constraints tco" & _
" join information_schema.key_column_usage kcu " & _
"     on kcu.constraint_name = tco.constraint_name" & _
"     and kcu.constraint_schema = tco.constraint_schema" & _
"     and kcu.constraint_name = tco.constraint_name" & _
" where tco.constraint_type = 'PRIMARY KEY' AND kcu.table_name = '"& $table_pk & "'" & _
" order by kcu.table_schema," & _
"         kcu.table_name," & _
"         position;"

;Query insert OR update
   $str = "DO $$ " & @CRLF & _
          "DECLARE " & @CRLF & _
		  "BEGIN IF EXISTS (SELECT * FROM #!#tabela#!# WHERE #!#PK#!# = #!#valorPK#!#) THEN " & @CRLF & _
		  "UPDATE #!#tabela#!# " & @CRLF & _
		  "SET #!#valorupdate#!# " & @CRLF & _
          "WHERE #!#PK#!# = '#!#valorPK#!#'; " & @CRLF & _
    "ELSE  " & @CRLF & _
        "INSERT INTO #!#tabela#!# (#!#colunas#!#) VALUES (#!#valores#!#); " & @CRLF & _
		"END IF; " & @CRLF & _
    "END $$;"

;Querry para apagar os dados ap�s copiar
$str_query_delete = "DELETE FROM #!#tabela#!# WHERE #!#PK#!# = #!#valorPK#!#"

ConsoleWrite(_NowTime ( 5 ) & " - MSSql >> Postgre - Copiando informa��es da tabela " & $tabela & @CRLF)

;Pega todas as informa��es da tabela
$msRS.Open( _
"SELECT * " & _
"FROM " & $table_name[2] & ";", _
$msConn, 1, 3)

;Consulta a PK
$pgPkRS.Open( $pgFindPkString, $pgPk, 1, 3)
$pgPkRS.MoveFirst()

;Escreve na v�riavel a PK
for $oField In $pgPkRS.Fields
   $TablePk = $oField.Value
next

;Lista todos os campos do DB
$fields = ""
for $oField In $msRS.Fields
$fields &= $oField.Name & ","
next

;Registra na variavel todas as colunas
$fields = StringTrimRight($fields, 1)

;Pega os valores do MSSQL e escreve no Postgre
If ((Not $msRS.EOF) AND (Not $msRS.BOF)) Then

;Vai para o primeiro registro do recordset
$msRS.MoveFirst()

;Cria as variaveis de valor e PK
$valor = ""
$PKvalor = ""

;Percorre todo o DB MSsql pegando o valor de cada linha e montando um insert or update
While Not $msRS.EOF
   ;Variavel do insert
   $valor = ""
   ;Variavel do UPDATE
   $updateString = ""

   ;Faz o loop das colunas para achar o valor da PK e montar a string de update
   for $oField In $msRS.Fields

	  ;Converte o valor do DB de origem para o de destino
	  $convertedValue = ConvertField( $oField )

	  ;Verifica se a coluna atual � a PK
	  if $oField.Name == $TablePk Then
		 ;Registra qual o valor da PK
		 $PKvalor = $convertedValue
	  else
		 ;Monta a string com os valores de update
		 $updateString &= $oField.Name & " = " & $convertedValue & ","
	  EndIf

	  ;Monta a string com os valores
	  $valor &= $convertedValue & ","

   next

   ;Remove as virgulas
   $valor = StringTrimRight ( $valor, 1 )
   $updateString = StringTrimRight ( $updateString, 1 )

   ;Substitui as variaveis da querry pelos valores da consulta
   $query = StringReplace($str,   "#!#tabela#!#", $tabela)
   $query = StringReplace($query, "#!#PK#!#", $TablePk)
   $query = StringReplace($query, "#!#valorPK#!#", $PKvalor)
   $query = StringReplace($query, "#!#valorupdate#!#", $updateString)
   $query = StringReplace($query, "#!#colunas#!#", $fields)
   $query = StringReplace($query, "#!#valores#!#", $valor)

   ;Substitui as variaveis da querry delete
   $query_delete = StringReplace($str_query_delete,   "#!#tabela#!#", $table_name[2])
   $query_delete = StringReplace($query_delete, "#!#PK#!#", $TablePk)
   $query_delete = StringReplace($query_delete, "#!#valorPK#!#", $PKvalor)

   ;Escreve a informa��o na tabela de destino
   ConsoleWrite(_NowTime ( 5 ) & " - Querry:"& @CRLF & $query & @CRLF)
   $pgConn.execute ( $query )

   ;Apaga informa��es da tabela espelho
   ConsoleWrite(_NowTime ( 5 ) & " - Querry:"& @CRLF & $query_delete & @CRLF)
   $msConn_del.execute ( $query_delete )

   ;Vai para o pr�ximo registro
   $msRS.MoveNext()

WEnd ;Enquanto houver registros pra escrever

EndIf ;IF verficando se tem informa��es na consulta

;Fecha os RecordSets
$pgPkRS.Close

$msRS.Close

Next ;Next para pr�xima tabela

EndIf ;IF verificando se � para fazer a sincroniza��o bidirecional
;--------------------------------------------------------------------------------------------------------------------------------------------------
;-------------------------------------------------------------FIM do c�digo------------------------------------------------------------------------
;--------------------------------------------------------------------------------------------------------------------------------------------------

;Fecha a conex�o com ambos os DB's
$pgConn.Close
$msConn.Close

ConsoleWrite(_NowTime ( 5 ) & " - Ciclo conclu�do esperando tempo para novo ciclo " & @CRLF)
;Espera 1 segundo antes de iniciar um novo ciclo
Sleep( 20000 )

WEnd

;Cria uma janela de aviso em caso de erro de comunica��o
 Func ErrFunc()
  $HexNumber=hex($oMyError.number,8)
  Msgbox($MB_ICONERROR,"Erro COM","Aconteceu um erro ao transferir informa��es do DB Postgre para o DB MSSql !" & @CRLF  & @CRLF & _
             "Descri��o do erro: "    & @CRLF & $oMyError.description   & @CRLF & @CRLF & _
             "C�digo do erro: "       & @TAB  & $HexNumber              & @CRLF & _
             "Linha do erro: "     	  & @TAB  & $oMyError.scriptline    & @CRLF & _
             "Fonte do erro: "        & @TAB  & $oMyError.source _
            )

   $Mensagem = "Aconteceu um erro ao transferir informa��es do DB Postgre para o DB MSSql !" & @CRLF & _
   @TAB & "Descri��o do erro: "    & @CRLF & @TAB & $oMyError.description & @CRLF & _
   @TAB & "Windescription:"        & @TAB  & $oMyError.windescription     & @CRLF & _
   @TAB & "C�digo do erro: "       & @TAB  & $HexNumber                   & @CRLF & _
   @TAB & "Lastdllerror: "         & @TAB  & $oMyError.lastdllerror       & @CRLF & _
   @TAB & "Linha do erro: "        & @TAB  & $oMyError.scriptline         & @CRLF & _
   @TAB & "Fonte do erro: "        & @TAB  & $oMyError.source		      & @CRLF & _
   @TAB & "Helpfile: "             & @TAB  & $oMyError.helpfile           & @CRLF & _
   @TAB & "Helpcontext: "          & @TAB  & $oMyError.helpcontext		  & @CRLF & _
   $str
   _FileWriteLog(@ScriptDir & "\Log_Gerenciador.log", $Mensagem, 1)

   Exit
Endfunc

;Fun��o que formata o valor conforme o tipo da vari�vel
Func ConvertField($Field)
   ;ConsoleWrite ($Field.Name & " Type: " & $Field.Type & " = " & $Field.Value  & @CRLF)
   Switch $Field.Type
	  CASE 0 ;Tipo adEmpty - No value
		 Return $Field.Value
	  CASE 2 ;Tipo adSmallInt - A 2-byte signed integer.
		 If _IsVarIsNull ( $Field.Value ) Then
			Return 0
		 Else
			Return $Field.Value
		 EndIf
	  CASE 3 ;Tipo adInteger - A 4-byte signed integer.
		 if $Field.name = 'cod_cliente' then
			return 'NULL'
			else
		 Return Number($Field.Value)
		 EndIf
	  CASE 4 ;Tipo adSingle - A single-precision floating-point value.
		 Return $Field.Value
	  CASE 5 ;Tipo adDouble - A double-precision floating-point value.
		 Return $Field.Value
	  CASE 6 ;Tipo adCurrency - A currency value
		 Return $Field.Value
	  CASE 7 ;Tipo adDate - The number of days since December 30, 1899 + the fraction of a day.
		 Return $Field.Value
	  CASE 8 ;Tipo adBSTR - A null-terminated character string.
		 Return "'" & StringReplace($Field.Value,"'","") & "'"
	  CASE 9 ;Tipo adIDispatch - A pointer to an IDispatch interface on a COM object. Note: Currently not supported by ADO.
		 Return $Field.Value
	  CASE 10 ;Tipo adError - A 32-bit error code
		 Return $Field.Value
	  CASE 11 ;Tipo adBoolean - A boolean value.
		 Return $Field.Value
	  CASE 12 ;Tipo adVariant - An Automation Variant. Note: Currently not supported by ADO.
		 Return $Field.Value
	  CASE 13 ;Tipo adIUnknown - A pointer to an IUnknown interface on a COM object. Note: Currently not supported by ADO.
		 Return $Field.Value
	  CASE 14 ;Tipo adDecimal - An exact numeric value with a fixed precision and scale.
		 Return $Field.Value
	  CASE 16 ;Tipo adTinyInt - A 1-byte signed integer.
		 Return $Field.Value
	  CASE 17 ;Tipo adUnsignedTinyInt - A 1-byte unsigned integer.
		 Return $Field.Value
	  CASE 18 ;Tipo adUnsignedSmallInt - A 2-byte unsigned integer.
		 Return $Field.Value
	  CASE 19 ;Tipo adUnsignedInt - A 4-byte unsigned integer.
		 Return $Field.Value
	  CASE 20 ;Tipo adBigInt - An 8-byte signed integer.
		 Return $Field.Value
	  CASE 21 ;Tipo adUnsignedBigInt - An 8-byte unsigned integer.
		 Return $Field.Value
	  CASE 64 ;Tipo adFileTime - The number of 100-nanosecond intervals since January 1,1601
		 Return $Field.Value
	  CASE 72 ;Tipo adGUID - A globally unique identifier (GUID)
		 Return $Field.Value
	  CASE 128 ;Tipo adBinary - A binary value.
		 Return $Field.Value
	  CASE 129 ;Tipo adChar - A string value.
		 Return "'" & StringReplace($Field.Value,"'","") & "'"
	  CASE 130 ;Tipo adWChar - A null-terminated Unicode character string.
		 Return "'" & StringReplace($Field.Value,"'","\'") & "'"
	  CASE 131 ;Tipo adNumeric - An exact numeric value with a fixed precision and scale.
		 If _IsVarIsNull ( $Field.Value ) Then
			Return 0
		 Else
			Return $Field.Value
		 EndIf
	  CASE 132 ;Tipo adUserDefined - A user-defined variable.
		 Return $Field.Value
	  CASE 133 ;Tipo adDBDate - A date value (yyyymmdd).
		 Return "convert(datetime,cast('"& StringLeft ( $Field.Value, 8 ) & "' as char(8)) )"
	  CASE 134 ;Tipo adDBTime - A time value (hhmmss).
		 Return "convert(datetime,cast('"& StringLeft ( $Field.Value, 8 ) & "' as char(8)) )"
	  CASE 135 ;Tipo adDBTimeStamp - A date/time stamp (yyyymmddhhmmss plus a fraction in billionths).
		 Return $Field.Value
	  CASE 136 ;Tipo adChapter - A 4-byte chapter value that identifies rows in a child rowset
		 Return $Field.Value
	  CASE 138 ;Tipo adPropVariant - An Automation PROPVARIANT.
		 Return $Field.Value
	  CASE 139 ;Tipo adVarNumeric - A numeric value (Parameter object only).
		 Return $Field.Value
	  CASE 200 ;Tipo adVarChar - A string value (Parameter object only).
		 Return "'" & StringReplace($Field.Value,"'","") & "'"
	  CASE 201 ;Tipo adLongVarChar - A long string value.
		 Return "'" & StringReplace($Field.Value,"'","") & "'"
	  CASE 202 ;Tipo adVarWChar - A null-terminated Unicode character string.
		 Return "'" & StringReplace($Field.Value,"'","") & "'"
	  CASE 203 ;Tipo adLongVarWChar - A long null-terminated Unicode string value.
		 Return "'" & StringReplace($Field.Value,"'","") & "'"
	  CASE 204 ;Tipo adVarBinary - A binary value (Parameter object only).
		 Return $Field.Value
	  CASE 205 ;Tipo adLongVarBinary - A long binary value.
		 Return $Field.Value

   EndSwitch

EndFunc

;Verifica se a informa��o � nula
Func _IsVarIsNull ( $_Var )
    If Not $_Var Then
        Return True
    Else
        Return False
    EndIf
EndFunc